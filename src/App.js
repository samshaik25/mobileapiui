import { BrowserRouter ,Route} from 'react-router-dom';

import './App.css';
import GetBrands from './brands/getBrands';
import Navbarr from './navbar';
import GetProducts from './products/getProducts';

function App() {
  return (
    <BrowserRouter basename='/BRANDS'>

    <div className="App">
      <Navbarr/>
     <Route   exact path='/' component={GetBrands}>
     <GetBrands />
     </Route>
     
     <Route className="product" path='/:id' component={GetProducts} />
     </div>
  
    </BrowserRouter>
  );

}

export default App;
