import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
// import FeatherIcon from '';
// import FeatherIcon from 'react-feather';

// import "./brand.css"
import './product.css'


class Product extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = { 
  //     Mobile_Name:'',
  //     Price:'',
  //     RAM:'',
  //     Internal_Storage:''
  //    }
  // }
  // onHandleChangeU=(event)=>{
  //   console.log("value"+event.target.value,"name"+event.target.name)
  //   this.setState({
  //       [event.target.name]:event.target.value
  
  //   })
  // }
  render() { 
    const {mobileName,RAM,Price,id,Internal_Storage,mcId,onDeleteProduct}=this.props
  
  

 
  return(
    
    <div className=" d-flex Brand card m-4"  >
    
    <Card bg="dark" border="light"  style={{ width: '20rem' }}>
    <div className="d-flex justify-content-between" >
    
    <Card.Header  className=" p-4" text="success"><h2  > <h3 className="text">{mobileName}</h3></h2></Card.Header>
    <button onClick={()=>onDeleteProduct(id)} style={{height:40}}  className='btn btn-sm btn-danger'>x</button>
    
    
    
    </div>
    <Card.Body>
    <Card.Title  className=" p-2" ><h4 className="  d-flex  flex-row"><span className="text" >RAM :</span>   <h4 className="text">{RAM} </h4></h4></Card.Title>
    <Card.Title  className=" p-2" ><h4 className="  d-flex  flex-row"><span className="text" >Price :</span>   <h4 className="text">{Price} </h4></h4></Card.Title>
    <Card.Title  className=" p-2" ><h4 className="  d-flex  flex-row"><span  className="text" >Storage :</span>   <h4 className="text">{Internal_Storage} </h4></h4></Card.Title>
    
    
    <Card.Text   >
    </Card.Text>
    </Card.Body>
    
    </Card>
    
    </div>
    
    
    )
  }
}
  
  // export default Brand
  // <FeatherIcon icon="x-circle" size="24" onClick={()=>onDeleteBrand(id)}/>
  // <button onClick={()=>onDeleteBrand(id)} style={{height:40}}  className='btn btn-sm btn-danger'>x</button>
  //   <Link className="Link" to={`${id}`} key={id}>
  //   <button >pro</button>
  //   </Link>
  export default Product;