import React, { Component } from 'react';

import * as API from '../api'

import Product from './product';
import CreateProduct from './createProduct';
class GetProducts extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            products:[],
            Mobile_Name:'',
            Price:'',
            RAM:'',
            Internal_Storage:''
        }
    }

    handleInputChange=(event)=>{
        this.setState({
            [event.target.name]:event.target.value
    
        })
    }
  
     handleOnSubmit=async(event)=>{
        event.preventDefault()
        const response=await API.postProduct(this.state,this.props.match.params.id)
        this.setState({products:[response,...this.state.products],     
        Price:"",
        RAM:"",
        Internal_Storage:"",
        Mobile_Name:''})
    
      }

      handleDelete=async(id)=>{
        const products=await API.deleteProduct(id)
        this.setState({products:this.state.products.filter(b=>b.id!=id)})
    }
  

   async getProducts(id){
      const products= await API.fetchProducts(id)
       this.setState({products}) 
    }

    componentDidMount()
    {
      let {id}=this.props.match.params;
      console.log(id)
      this.getProducts(id)
    }

    render() { 

        const {products,Mobile_Name,Price,RAM,Internal_Storage}=this.state
        return (
            
       <div className="productI">

       <CreateProduct  onHandleChange={this.handleInputChange} onHandleSubmit={this.handleOnSubmit}
       Mobile_Name={Mobile_Name} Price={Price} RAM={RAM} Internal_Storage={Internal_Storage} />


       <div className="d-flex flex-row flex-wrap justify-content-center align-items-center">
       {products.map(({Mobile_Name,RAM,Price,id,Internal_Storage,mobileCompanyId})=>(
           <Product mobileName={Mobile_Name} RAM={RAM} Price={Price}
           Internal_Storage={Internal_Storage} mcId={mobileCompanyId} id={id} key={id}
           onDeleteProduct={this.handleDelete}
           />
           ))}
           </div>
           </div>
           );
        }
    }
    
    export default GetProducts;