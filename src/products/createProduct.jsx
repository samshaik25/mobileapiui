import React, { Component } from 'react';


export default function CreateProduct({Mobile_Name,Price,RAM,Internal_Storage,onHandleChange,onHandleSubmit})
{
  return(
    <form onSubmit={onHandleSubmit}>
    <div className="d-flex flex-column flex-wrap justify-content-center align-items-center">
    <div>
    <label htmlFor style={{color:'white',fontSize:500}}><h2 >ADD Product</h2></label>
    </div>
    <div>
    <input
    value={Mobile_Name}
    onChange={onHandleChange}
    name="Mobile_Name"
    placeholder="Mobile_Name "
    />  
    <input
    value={Price}
    onChange={onHandleChange}
    name="Price"

    placeholder="Price"
    />
    
    <input 
    value={RAM}
    onChange={onHandleChange}
    name="RAM"
    placeholder="RAM"
    />
    
    <input
    value={Internal_Storage}
    onChange={onHandleChange}
    name="Internal_Storage"
    placeholder="Internal storage"
    />
    <button type="submit" className="btn btn-primary"> Submit </button>
    </div>
    </div>    
    </form>

    
  )


}