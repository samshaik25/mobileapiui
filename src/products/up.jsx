import React, { Component } from 'react';


export default function CreateBrand({CEO,Headquaters,Company,Revenue,onHandleChange,onHandleSubmit})
{
  return(
    <form onSubmit={onHandleSubmit}>
    <div className="d-flex flex-column flex-wrap justify-content-center align-items-center">
    <div>
    <label htmlFor style={{color:'white',fontSize:500}}><h2 >ADD BRAND</h2></label>
    </div>
    <div>
    <input
    value={Company}
    onChange={onHandleChange}
    name="Company"
    placeholder="Brand "
    />  
    <input
    value={CEO}
    onChange={onHandleChange}
    name="CEO"
    label="position"
    placeholder="CEO"
    />
    
    <input 
    value={Headquaters}
    onChange={onHandleChange}
    name="Headquaters"
    label="link"
    placeholder="Headquater"
    />
    
    <input
    value={Revenue}
    onChange={onHandleChange}
    name="Revenue"
    placeholder="Revenue"
    />
    <button type="submit" className="btn btn-primary"> Submit </button>
    </div>
    </div>    
    </form>

    
  )


}