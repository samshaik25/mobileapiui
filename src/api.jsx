import axios from 'axios'


export function fetchBrands(){

    return axios.get("https://mobile-database.herokuapp.com/mobiles/brands")
    .then(res=>res.data)
}


export function postBrand(state)
{

    return axios.post("https://mobile-database.herokuapp.com/mobiles/brand",state)
    .then(res=>res.data)
}


export function deleteBrand(id)
{
    return axios.delete(`https://mobile-database.herokuapp.com/mobiles/brand/${id}`)
    .then((res=>res.data))

}


export function updateBrand(id,body){

    return axios.put(`https://mobile-database.herokuapp.com/mobiles/brand/${id}`,body).
    then((res)=>res.data)

}

export function fetchProducts(id)
{
    return axios.get(`https://mobile-database.herokuapp.com/mobiles/products/${id}`).
    then((res)=>res.data)

}


export function postProduct(state,id)
{

    return axios.post(`https://mobile-database.herokuapp.com/mobiles/product/${id}`,state)
    .then(res=>res.data)
}

export function deleteProduct(id)
{
    return axios.delete(`https://mobile-database.herokuapp.com/mobiles/product/${id}`)
    .then((res=>res.data))

}