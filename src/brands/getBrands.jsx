import React, { Component } from 'react';

import * as API from '../api'
import './brand.css'
import Brand from './brand';
import CreateBrand from './createBrand';



class  GetBrands extends Component {
    constructor(props) {
        super(props);
        this.state = {  
            brands:[],
            Company:"",
            CEO: "",
         Headquaters: "",
         Revenue:""
        }
    }

    async getBrands()
    {
        const brands= await API.fetchBrands()
        this.setState({brands})
    }

    handleInputChange=(event,id)=>{
        console.log("value"+event.target.value,"name"+event.target.name,id)
        this.setState({
            [event.target.name]:event.target.value
    
        })
    }
  
  
     handleOnSubmit=async(event)=>{
        event.preventDefault()
        const response=await API.postBrand(this.state)
        // console.log(this.state.brands)
        this.setState({brands:[response,...this.state.brands], Company:"",
        CEO: "",
     Headquaters: "",
     Revenue:""})
    
      }


      handleOnSubmitUp=async(event,id,statee)=>{
        event.preventDefault()
        console.log("id",id)
        console.log("state",statee)
        const response=await API.updateBrand(id,statee)
        const {brands}=this.state
        console.log( response)
        // console.log(this.state.brands)
            this.setState({brands})
      }
    
    componentDidMount()
  {
   this.getBrands();
   
  }

  handleDelete=async(id)=>{
      const brands=await API.deleteBrand(id)
      this.setState({brands:this.state.brands.filter(b=>b.id!=id)})
  }
Monclick=(id)=>{
    console.log("iddddd",id)
}



    render() { 
        const {brands,Company,CEO,Headquaters,Revenue,}=this.state

        return ( 
            <div>
       <CreateBrand  onHandleChange={this.handleInputChange} onHandleSubmit={this.handleOnSubmit}
       CEO={CEO} Company={Company} Headquaters={Headquaters} Revenue={Revenue} />

            <div className="d-flex flex-row flex-wrap justify-content-center align-items-center">
            {brands.map(({Company,id,CEO,Revenue,Headquaters})=>(

                <Brand key={id} brands={brands} Company={Company} id={id} CEO={CEO} Headquaters={Headquaters} 
                Revenue={Revenue} onHandleChangeU={this.handleInputChangeUp}
                onHandleSubmitU={this.handleOnSubmitUp} Monclick={this.Monclick} onDeleteBrand={this.handleDelete}
                />
                
                ))}

                </div>
                
               
                </div>
                );
    }
}
 
export default GetBrands ;